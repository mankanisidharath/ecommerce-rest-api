@component('mail::message')
# Hello, {{ $user->name }}

Thankyou for creating an account with us. Please verify your email by clicking the following link. If you have any problem in verification you can contact us on 'admin@ecommerce-rest.com'

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
