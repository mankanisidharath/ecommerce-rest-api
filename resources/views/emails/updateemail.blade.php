@component('mail::message')
# Hello, {{ $user->name }}

Your email has been updated, Please verify your email by clicking the following link.

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
