<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApplicationSignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $header = 'X-name')
    {
        // Before Middleware code is here when you want to process something with the request
        // return $next($request);
        // After return to the next middleware, if you want to process something with the response you can do it here and it is called as After Middleware. just remove the return statement catch the response and return after processing the response.
        $response = $next($request);
        $response->headers->set($header, config('app.name'));
        return $response;
    }
}
